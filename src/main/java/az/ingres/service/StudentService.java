package az.ingres.service;

import az.ingres.dto.StudentDto;
import az.ingres.model.Student;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;

import java.util.List;

public interface StudentService {
    List<StudentDto> findAll();

    StudentDto get(Integer id);

    StudentDto create(StudentDto student);

    StudentDto update(Integer id, Student student);

    void delete(Integer id);

}

