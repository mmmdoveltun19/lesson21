package az.ingres.mapper;

import az.ingres.dto.StudentDto;
import az.ingres.dto.StudentDto.StudentDtoBuilder;
import az.ingres.model.Student;
import az.ingres.model.Student.StudentBuilder;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.processing.Generated;
import org.springframework.stereotype.Component;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2023-12-21T20:45:37+0400",
    comments = "version: 1.4.2.Final, compiler: javac, environment: Java 20.0.2.1 (Amazon.com Inc.)"
)
@Component
public class StudentMapperImpl implements StudentMapper {

    @Override
    public StudentDto entityToDto(Student student) {
        if ( student == null ) {
            return null;
        }

        StudentDtoBuilder studentDto = StudentDto.builder();

        studentDto.name( student.getName() );
        studentDto.lastname( student.getLastname() );
        studentDto.age( student.getAge() );

        return studentDto.build();
    }

    @Override
    public Student dtoToEntity(StudentDto student) {
        if ( student == null ) {
            return null;
        }

        StudentBuilder student1 = Student.builder();

        student1.id( student.getId() );
        student1.name( student.getName() );
        student1.lastname( student.getLastname() );
        student1.age( student.getAge() );

        return student1.build();
    }

    @Override
    public List<StudentDto> listEntityToDto(List<Student> student) {
        if ( student == null ) {
            return null;
        }

        List<StudentDto> list = new ArrayList<StudentDto>( student.size() );
        for ( Student student1 : student ) {
            list.add( entityToDto( student1 ) );
        }

        return list;
    }
}
