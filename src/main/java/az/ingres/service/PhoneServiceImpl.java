package az.ingres.service;

import az.ingres.dto.PhoneDto;
import az.ingres.model.Phone;
import az.ingres.model.Student;
import az.ingres.repository.StudentRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class PhoneServiceImpl implements PhoneService{
    private final StudentRepository studentRepository;

    @Override
    public PhoneDto create(Integer studentId, PhoneDto phoneDto) {
        Student student = studentRepository.findById(studentId).orElseThrow(RuntimeException::new);
        Phone phone = Phone.builder()
                .number(phoneDto.getNumber())
                .build();
        studentRepository.save(student);
        return null;
    }
}
