package az.ingres.controller;


import az.ingres.dto.StudentDto;
import az.ingres.model.Student;
import az.ingres.service.StudentService;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/student/v1")
public class StudentController {

    private final StudentService studentService;

    public StudentController(StudentService studentService) {
        this.studentService = studentService;
    }

    @GetMapping("/{id}")
//    @Cacheable(cacheNames = "student",key = "#id")
    public StudentDto get(@PathVariable Integer id) {
        return studentService.get(id);
    }

    @GetMapping("/static")
    @Transactional
    public StudentDto method() throws InterruptedException {
        Thread.sleep(1000);
        return StudentDto.builder().build();
    }

    @GetMapping("/all")
    public List<StudentDto> all() {
        return studentService.findAll();
    }

    @PostMapping
    public StudentDto create(@RequestBody StudentDto student) {
        return studentService.create(student);
    }

    @PutMapping("/{id}")
    public StudentDto update(@PathVariable Integer id, @RequestBody Student student) {
        return studentService.update(id, student);
    }

    @DeleteMapping("/{id}")
    public void delete(@PathVariable Integer id) {
        studentService.delete(id);
    }
}

