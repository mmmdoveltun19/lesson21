FROM openjdk:17-alpine
ARG AZERICARD_URL
ENV url=$AZERICARD_URL
CMD ["sh", "-c", "echo $url"]
