package az.ingres;

import az.ingres.model.Student;
import az.ingres.repository.AccountRepository;
import az.ingres.repository.StudentRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import org.springframework.cache.CacheManager;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.data.redis.core.RedisTemplate;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;


@SpringBootApplication
@RequiredArgsConstructor
@Slf4j
@EnableCaching
public class SpringDemoMs19Application implements CommandLineRunner {


	private final CacheManager cacheManager;
	private final RedisTemplate<String,String> redisTemplate;
	private final RedisTemplate<String,Object> redisTemplate2;
	private final AccountRepository accountRepository;
	private final StudentRepository studentRepository;

	public static void main(String[] args) {
		SpringApplication.run(SpringDemoMs19Application.class, args);
	}


	@Override
	public void run(String... args) throws Exception {

		List<Student> a = new ArrayList<>();
		for (int i = 0;i < 10000;i++) {
			a.add(Student.builder()
							.id(UUID.randomUUID())
							.name("Ali")
							.lastname("Mammadov" + i)
							.age(i)
					.build());
		}
		studentRepository.saveAll(a);
		System.out.println("done");
	}
}