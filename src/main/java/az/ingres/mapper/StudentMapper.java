package az.ingres.mapper;

import az.ingres.dto.StudentDto;
import az.ingres.model.Student;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import java.util.List;

import static org.mapstruct.ReportingPolicy.IGNORE;

@Mapper(componentModel = "spring", unmappedTargetPolicy = IGNORE)
public interface StudentMapper {


    @Mapping(target = "id", ignore = true)
    StudentDto entityToDto(Student student);
    Student dtoToEntity(StudentDto student);

    List<StudentDto> listEntityToDto(List<Student> student);
}

