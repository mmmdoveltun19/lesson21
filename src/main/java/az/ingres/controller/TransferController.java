package az.ingres.controller;

import az.ingres.model.Account;
import az.ingres.repository.AccountRepository;
import az.ingres.service.TransferService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/transfer/v1")
@RequiredArgsConstructor
@Slf4j
public class TransferController {

    private final AccountRepository accountRepository;
    private final TransferService transferService;

    @GetMapping("{id}")
    public Account get(@PathVariable Integer id) {
        return accountRepository.findById(id).get();
    }

    @PutMapping
    @Transactional
    public void transfer() {
        log.info("Getting account with id 1");
        Account from = accountRepository.findById(1).get(); //280
        log.info("FROM account is {}",from);
        log.info("Getting account with id 1");
        Account to = accountRepository.findById(2).get(); //300
        log.info("TO account is {}",to);
        transferService.transfer(from,to,20);
    }
    @PutMapping("/withdraw/amount")
    @Transactional
    public void withdraw(@PathVariable Integer amount) {
        log.info("WITHDRAW:Getting account with id 1");
        Account from = accountRepository.findById(1).get(); //0
        log.info("WITHDRAW:FROM account is {}",from);
        transferService.withdraw(from,amount);
    }
}