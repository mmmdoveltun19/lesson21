package az.ingres.repository;

import az.ingres.model.Student;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.Optional;

public interface StudentRepository extends JpaRepository<Student,Integer> {

//    @Query(value = "select * from student where age = :age",nativeQuery = true)
//    List<Student> findStudentWithAgeEquals(Integer age);

    //JPQL - java persistence query language
//    @Query(value = "select s from Student s left join fetch s.phoneList")
//    @EntityGraph(value = "students_with_phone_list",type = EntityGraph.EntityGraphType.FETCH)
    List<Student> findAllBy();


    //class version
//    @Query(value = "select new  az.ingress.demo.repository.StudentGroupByDto (s,age, count(*)) from Student s group by s.age")
//    List<StudentGroupByDto> groupByAgeJpql();


    @Override
    Optional<Student> findById(Integer integer);
}

