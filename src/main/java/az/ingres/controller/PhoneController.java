package az.ingres.controller;

import az.ingres.dto.PhoneDto;
import az.ingres.service.PhoneService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/student/v1")
@RequiredArgsConstructor
public class PhoneController {

    private final PhoneService phoneService;

    @PostMapping("/{studentId}/phone")
    public PhoneDto create(@PathVariable Integer studentId, @RequestBody PhoneDto phoneDto) {
        return phoneService.create(studentId,phoneDto);
    }

    @DeleteMapping("/{studentId}/phone")
    public PhoneDto delete(@PathVariable Integer studentId,@RequestBody PhoneDto phoneDto) {
        return phoneService.create(studentId,phoneDto);
    }
}

