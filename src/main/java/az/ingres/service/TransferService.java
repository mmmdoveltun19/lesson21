package az.ingres.service;

import az.ingres.model.Account;
import az.ingres.repository.AccountRepository;
import jakarta.persistence.EntityManager;
import jakarta.persistence.EntityManagerFactory;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.apache.catalina.core.ApplicationContext;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@Service
@Slf4j
@RequiredArgsConstructor
public class TransferService {

    private final AccountRepository accountRepository;
    private final EntityManagerFactory emf;
    private final ApplicationContext context;

    @SneakyThrows
    @Transactional(propagation = Propagation.NESTED)
    public void transfer(Account from, Account to, double amount) {//from=280 ,to=300
        if (from.getBalance() < amount) {
            throw new RuntimeException("Not Enough balance");
        }
        Thread.sleep(3000);
        from.setBalance(from.getBalance() - amount);
        to.setBalance(to.getBalance() + amount);
        log.info("FROM balance is {}", from.getBalance());
        log.info("TO balance is {}", to.getBalance());
        log.info("trying to save from");
        accountRepository.save(from);
        log.info("trying to save to");
        accountRepository.save(to);
    }


    public void withdraw(Account from, Integer amount) {
        if (from.getBalance() < amount) {
            throw new RuntimeException("Not Enough balance");
        }
        from.setBalance(from.getBalance() - amount);
        log.info("WITHDRAW:trying to save from");
        accountRepository.save(from);
    }

    @Transactional(isolation = Isolation.SERIALIZABLE)
    public void transfer2(Account from, Account to, double amount, EntityManager entityManager) {//240 //330
        if (from.getBalance() < amount) {
            throw new RuntimeException();
        }
        from.setBalance(from.getBalance() - amount);
        to.setBalance(to.getBalance() + amount);
        entityManager.merge(from);
        if (true) {
            throw new RuntimeException();
        }
        entityManager.merge(to);
    }


    public void hello() {

    }

    @Transactional
    public void proxyTransfer(Account from, Account to, double amount) {
        //begin
        EntityManager entityManager = emf.createEntityManager();
        entityManager.getTransaction().begin();
        hello();
        try {
            transfer2(from, to, amount, entityManager);
            entityManager.getTransaction().commit();
        } catch (NullPointerException e) {
            entityManager.getTransaction().commit();
        } catch (RuntimeException e) {
            entityManager.getTransaction().rollback();
        } catch (Exception e) {
            entityManager.getTransaction().commit();

        } finally {
            entityManager.close();

        }
    }
    //commit
}