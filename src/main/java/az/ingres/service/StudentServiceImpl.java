package az.ingres.service;

import az.ingres.config.Config;
import az.ingres.dto.StudentDto;
import az.ingres.mapper.StudentMapper;
import az.ingres.model.Student;
import az.ingres.repository.StudentRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cache.Cache;
import org.springframework.cache.CacheManager;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import java.util.List;

@Slf4j
@Service
@RequiredArgsConstructor
public class StudentServiceImpl implements StudentService {

    private final StudentRepository studentRepository;
    private final Config config;
    private final StudentMapper studentMapper;
    private final CacheManager cacheManager;


    @Override
//    @Cacheable(value = "student")
    public List<StudentDto> findAll() {
        return studentRepository.findAll().stream().map(studentMapper::entityToDto).toList();
    }

    @Override
//    @Cacheable(key = "#id",cacheNames = "student")
    public StudentDto get(Integer id) {
        Cache cache = cacheManager.getCache("login");
        cache.put(id,true);
        Student student = studentRepository.findById(id).orElseThrow(() -> new RuntimeException("Student not found"));
        return studentMapper.entityToDto(student);
    }

    @Override
    public StudentDto create(StudentDto studentDto) {
        log.info("Student service create method is working");
        Student student = studentMapper.dtoToEntity(studentDto);
        studentRepository.save(student);
        return studentMapper.entityToDto(student);
    }

    @Override
//    @CachePut(key = "#student.age",cacheNames = {"aaaa","bbbb"})
    public StudentDto update(Integer id, Student student) {
        log.info("Student service update method is working");
        Student entity = studentRepository.findById(id)
                .orElseThrow(() -> new RuntimeException("Student not found"));
        entity.setAge(student.getAge());
        entity.setName(student.getName());
        entity.setLastname(student.getLastname());
        entity = studentRepository.save(entity);
        return studentMapper.entityToDto(student);
    }

    @Override
//    @CacheEvict(key = "#id",cacheNames = "student")
    public void delete(Integer id) {
        log.info("Student service delete method is working");
        studentRepository.deleteById(id);
    }
}