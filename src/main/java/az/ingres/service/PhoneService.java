package az.ingres.service;

import az.ingres.dto.PhoneDto;

public interface PhoneService {
    PhoneDto create(Integer studentId, PhoneDto phoneDto);
}

