package az.ingres.repository;

public interface StudentGroupByDto {
    Integer getAge();
    Long getCount();
}




