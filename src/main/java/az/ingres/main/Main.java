package az.ingres.main;

import java.lang.ref.PhantomReference;
import java.lang.ref.ReferenceQueue;

public class Main {

    public static void main(String[] args) {
        ReferenceQueue<Student> referenceQueue = new ReferenceQueue();

        PhantomReference<Student> phantomReference = new PhantomReference<>(new Student(),referenceQueue);
    }
}

